//
//  Request.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 03.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

/*
  leonzawodowiec@gmail.com;Qwerty123
  hilary.narecki@vp.pl;Qwerty123
  irmina.naczko@vp.pl;Qwerty123
  emilia.jachta@vp.pl;Qwerty123
  jankowalski@gmail.com;Qwerty123
  jerzykoro24@vp.pl;Qwerty123
 */

import Alamofire

class BaseRequest {
    //static let baseUrl = "http://web-kpawlik.kotek.xara.ovh:8083/api"
    static let baseUrl = "https://api.kotstudent.xara.ovh"
    static let baseHeader: [String : String] = ["Content-Type" : "application/json"]
    
    static func setCookies(cookies: [HTTPCookie], response: HTTPURLResponse){
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: response.url!, mainDocumentURL: nil)
    }
    
    static func removeCookies(){
        let cstorage = HTTPCookieStorage.shared
        if let cookies = cstorage.cookies {
            for cookie in cookies {
                cstorage.deleteCookie(cookie)
            }
        }
    }
    
    static func isCookies() -> Bool{
        let cstorage = HTTPCookieStorage.shared.cookies(for: NSURL(string: baseUrl)! as URL)
        
        if(cstorage?.isEmpty)! {
            return false
        }
        
        return true
    }
}
