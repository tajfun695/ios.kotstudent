//
//  AppViewService.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 15.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Alamofire
import SwiftyJSON

class AppViewService: BaseRequest {
    static func getMethod(url: String, completion: @escaping (JSON?, String?) -> ()){
        Alamofire.request(baseUrl + url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: baseHeader)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    let json = JSON(value)
                    completion(json,nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(nil, error.localizedDescription)
                }
        }
    }
    
    static func otherMethod(url: String, method: HTTPMethod, parameters: JSON, completion: @escaping (JSON?, String?) -> ()) {
        Alamofire.request(baseUrl + url, method: method, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default, headers: baseHeader)
            .responseJSON { (result) in
                switch result.result {
                case .success(let value):
                    let json = JSON(value)
                    completion(json, nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(nil, error.localizedDescription)
                }
        }
    }
}
