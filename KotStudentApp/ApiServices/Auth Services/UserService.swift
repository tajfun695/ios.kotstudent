//
//  UserService.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 21.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Alamofire
import SwiftyJSON

class UserService: BaseRequest {
    static func getUser(url: String, completion: @escaping (Bool, String?) -> ()){
        Alamofire.request(baseUrl + url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: baseHeader)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    print(value)
                    completion(true, nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(false, nil)
                }
        }
    }
}
