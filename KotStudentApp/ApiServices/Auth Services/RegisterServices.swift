//
//  RegisterServices.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 17.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Alamofire
import SwiftyJSON

class RegisterService: BaseRequest {
    static func RegisterByEmail(url: String, username: String, email: String, password: String, completion: @escaping (Bool, String?) -> ()){
        let parameters : [String : String] = ["userName": username, "emailAddress": email , "plainPassword": password]
        Alamofire.request(baseUrl + url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: baseHeader)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    let status: String = String(describing:value)
                    
                    if(status.contains("Error") == false){
                        completion(true, nil)
                    } else {
                        let jsonError = JSON(value)["Error"]["identifier"].stringValue
                        switch jsonError{
                        case "email-address-already-in-use":
                            completion(false, "E-mail już jest zajęty.")
                        case "failed-create-user":
                            completion(false, "Wystąpił nieznany błąd uniemożliwiający utworzenie konta użytkownika.")
                        default:
                            completion(false, "Error: " + jsonError.description)
                        }
                    }
                case .failure(let error):
                    completion(false, error.localizedDescription)
                }
        }
    }
}
