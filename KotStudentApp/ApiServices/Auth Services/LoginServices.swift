//
//  Login-Register Services.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 17.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Alamofire
import SwiftyJSON


class LoginService: BaseRequest {
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //LoginByProviders!
    static func LoginByProvider(url: String, completion: @escaping (Bool, String?) -> ()) {
        Alamofire.request(baseUrl + url)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    let status: String = String(describing:value)
                    
                    if(status.contains("Error") == false){
                        let header = result.response?.allHeaderFields as? [String:String]
                        BaseRequest.setCookies(cookies: HTTPCookie.cookies(withResponseHeaderFields: header!, for: (result.response?.url)!), response: result.response!)
                        completion(true, nil)
                    } else {
                        let jsonError = JSON(value)["Error"]["identifier"].stringValue
                        switch jsonError{
                        case "facebook-access-token-failed", "invalid-facebook-access-token", "missed-access-token":
                            completion(false, "Autoryzacja niepowiodła się")
                        case "facebook-user-data-not-available":
                            completion(false, "Pobranie danych z konta Facebook niepowiodło się")
                        default:
                            completion(false, "Error: " + jsonError.description)
                        }
                    }
                case .failure(let error):
                    completion(false, error.localizedDescription)
                }
            }
    }
    
    //LoginByEmail
    static func LoginByEmail(url: String, emailAddress: String, password: String, completion: @escaping (Bool, String?) -> ()){
        let parameters : [String : String] = ["emailAddress": emailAddress, "plainPassword": password]
        Alamofire.request(baseUrl + url, method: .post, parameters: parameters, encoding: JSONEncoding.default , headers: baseHeader)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    let status: String = String(describing:value)
                    if(status.contains("Error") == false){
                        
                        let header = result.response?.allHeaderFields as? [String:String]
                        BaseRequest.setCookies(cookies: HTTPCookie.cookies(withResponseHeaderFields: header!, for: (result.response?.url)!), response: result.response!)
                        completion(true, nil)
                    } else {
                        let jsonError = JSON(value)["Error"]["identifier"].stringValue
                        switch jsonError{
                        case "user-not-found":
                            completion(false, "Nie jesteś jeszcze zarejestrowany")
                        case "invalid-email-address":
                            completion(false, "Niepoprawny e-mail")
                        case "invalid-password-error":
                            completion(false, "Nieprawidłowe hasło")
                        case "authorization-not-allowed":
                            completion(false, "Logowanie niepowiodło się, spróbuj poźniej")
                        default:
                            completion(false, "Error: " + jsonError.description)
                        }
                    }
                case .failure(let error):
                    completion(false, error.localizedDescription)
                }
            }
    }
}
