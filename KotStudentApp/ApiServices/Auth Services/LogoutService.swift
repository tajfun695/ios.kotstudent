//
//  LogoutService.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 21.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Alamofire

class LogoutService: BaseRequest {
    static func Logout(url: String, completion: @escaping (Bool, String?) -> ()){
        Alamofire.request(baseUrl + url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: baseHeader)
            .responseJSON { (result) in
                switch result.result{
                case .success(let value):
                    let status: String = String(describing:value)
                    
                    if(status.contains("Error") == false){
                        completion(true, nil)
                    } else {
                        completion(false, "Wylogowywanie niepowiodło się")
                    }
                case .failure(let error):
                    completion(false, error.localizedDescription)
                }
        }
    }
}
