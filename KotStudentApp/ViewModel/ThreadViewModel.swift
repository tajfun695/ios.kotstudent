//
//  SingielGroupViewModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 16.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift
import SwiftyJSON

class ThreadViewModel {
    
    let group: Variable<Group>
    let threads: Variable<[Thread]>
    
    init(){
        self.group = Variable(Group())
        self.threads = Variable([])
    }
    
    func GetComplesGroup(id:String, completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/\(id)/complex") { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            self.group.value = Group(json: json!["GroupComplexInfo"]["group"])
            
            for jsonThred in json!["GroupComplexInfo"]["threads"].arrayValue{
                self.threads.value.append(Thread(json: jsonThred))
            }
            
            completion(.success)
        }
    }
}
