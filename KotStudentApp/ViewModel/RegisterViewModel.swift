//
//  RegisterViewController.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 07.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift

class RegisterViewModel: NSObject {
    
    let email: Variable<String>
    let username: Variable<String>
    let password: Variable<String>
    
    override init(){
        self.email = Variable("")
        self.username = Variable("")
        self.password = Variable("")
    }
    
    public func RegisterUser() {
        
    }
}
