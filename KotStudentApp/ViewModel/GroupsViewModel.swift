//
//  GroupsViewModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 15.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift

class GroupsViewModel {
    
    let groups: Variable<[Group]>
    //let groups: Variable<[String]> = Variable(["fdsfsdfsdf"])
    init(){
        self.groups = Variable([])
    }
    
    func ReloadGroups(completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/my-groups") { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
                return
            }
            
            for jsonPart in json!["UserGroups"].arrayValue {
                let item = Group(json: jsonPart)
                self.groups.value.append(item)
            }
            completion(.success)
        }
    }
}
