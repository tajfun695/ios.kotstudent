//
//  PostViewModel.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 14.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift
import SwiftyJSON

class PostViewModel {
    
    let comments: Variable<[Comment]>
    
    init(){
        self.comments = Variable([])
    }
    
    func GetComment(threadId:Int, postId: Int, completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/threads/\(threadId)/posts/\(postId)/comments") { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            for jsonPost in json!["Comments"].arrayValue{
                self.comments.value.append(Comment(json: jsonPost))
            }
            
            completion(.success)
        }
    }
    
    static func putComment(threadID: Int, postID: Int, parameters: JSON, completion: @escaping (Result) -> ()) {
        AppViewService.otherMethod(url: "/group/threads/\(threadID)/posts/\(postID)/comments", method: .put, parameters: parameters) { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            
            if(json!["status"].boolValue == true){
                completion(.success)
                return
            }
            
            completion(.fail("Komentarz nie został dodany!"))
        }
    }
}
