//
//  GroupViewModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 01.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift
import SwiftyJSON

class GroupViewModel {
    let group: Variable<Group>
    let threads: Variable<[Thread]>
    let posts: Variable<[Post]>
    
    init(){
        self.group = Variable(Group())
        self.threads = Variable([])
        self.posts = Variable([])
    }
    
    func GetComplesGroup(groupId:Int, completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/\(groupId)/complex") { (json, error) in
            if error != nil || json!.stringValue.contains("Error") == true {
                print("Error. Metoda GetComplesGroup")
                completion(.fail("ERROR"))
            }
            
            self.group.value = Group(json: json!["GroupComplexInfo"]["group"])
            
            for jsonThred in json!["GroupComplexInfo"]["threads"].arrayValue{
                self.threads.value.append(Thread(json: jsonThred))
            }
            
            completion(.success)
        }
    }
    
    func GetPosts(threadId:Int, completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/threads/\(threadId)/posts") { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            for jsonPost in json!["ThreadPosts"].arrayValue{
                self.posts.value.append(Post(json: jsonPost))
            }
            
            completion(.success)
        }
    }
    
    func GetMorePosts(threadId:Int, lastPost: Int, completion: @escaping (Result) -> ()){
        AppViewService.getMethod(url: "/group/threads/\(threadId)/posts?startSearchPointer=\(lastPost)") { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            guard let jsonArray = json?["ThreadPosts"].arrayValue else { completion(.fail("Nie ma więcej postów!")); return }
            for jsonPost in jsonArray{
                self.posts.value.append(Post(json: jsonPost))
            }
            
            completion(.success)
        }
    }
    
    static func putPost(threadId: Int, parameters: JSON, completion: @escaping (Result) -> ()){
        AppViewService.otherMethod(url: "/group/threads/\(threadId)/posts", method: .put , parameters: parameters) { (json, error) in
            if error != nil{
                print(error!)
                completion(.fail("ERROR"))
            }
            
            if(json!["status"].boolValue == true){
                completion(.success)
            }
            
            completion(.fail("Post nie został dodany!"))
        }
    }
}
