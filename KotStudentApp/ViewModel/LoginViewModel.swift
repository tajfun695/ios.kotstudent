
//
//  LoginViewModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 24.02.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import RxSwift
import FBSDKLoginKit

class LoginViewModel {
    
    var email: Variable<String> = Variable("")
    var password: Variable<String> = Variable("")
    
    var isClickable: Observable<Bool>{
        return Observable.combineLatest(self.email.asObservable(), self.password.asObservable()){ email, password in
            return email.characters.count > 0 && password.characters.count > 0
        }
    }
    
    func LoginViaEmail(completion: @escaping (Result) -> ()) {
        BaseRequest.removeCookies()
        LoginService.LoginByEmail(url: "/auth/connect-email-address", emailAddress: email.value , password: password.value, completion: { result, error in
            DispatchQueue.main.async {
                if result != true {
                    NSLog("😡😡😡Logowanie nie powiodło się!")
                    completion(.fail(error!))
                    return
                }
                completion(.success)
            }
        })
    }

    func LoginViaFacebook(_ token: String, completion: @escaping (Result) -> ()) {
            BaseRequest.removeCookies()
            LoginService.LoginByProvider(url: "/auth/connect-with-facebook/\(token)", completion: { result, error in
                DispatchQueue.main.async {
                    if !result {
                        NSLog("😡😡😡 Logowanie nie powiodło się!")
                        self.LogoutFacebook()
                        completion(.fail(error!))
                        return
                    }
                    completion(.success)
                }
            })
        }

    func LogoutFacebook(){
       let logout = FBSDKLoginManager()
       logout.logOut()
   }
}

