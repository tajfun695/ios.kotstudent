//
//  Colors.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 03.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class Color {
    public static let mainColor: UIColor = UIColor(displayP3Red: 48/255, green: 63/255, blue: 159/255, alpha: 1.0)
    public static let secondaryColor: UIColor = UIColor(displayP3Red: 0/255, green: 145/255, blue: 234/255, alpha: 1)
    public static let background: UIColor = UIColor(displayP3Red: 233/255, green: 235/255, blue: 238/255, alpha: 1)
    public static let cellColor: UIColor = .white
    public static let lightGrey: UIColor = UIColor(displayP3Red: 211/255, green: 211/255, blue: 211/255, alpha: 1)
}
