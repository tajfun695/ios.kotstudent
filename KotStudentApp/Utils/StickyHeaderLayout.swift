//
//  StickyHeaderLayout.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 25.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class StickyHeadersLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttribute: [UICollectionViewLayoutAttributes] = super.layoutAttributesForElements(in: rect)!
        
        let headerNeedingLayout = NSMutableIndexSet()
        for attribute in layoutAttribute{
            if attribute.representedElementCategory == .cell {
                headerNeedingLayout.add(attribute.indexPath.section)
            }
        }
        
        for attribute in layoutAttribute {
            if let elementKind = attribute.representedElementKind {
                if elementKind == UICollectionElementKindSectionHeader {
                    headerNeedingLayout.remove(attribute.indexPath.section)
                }
            }
        }
        
        headerNeedingLayout.enumerate { (index, stop) in
            let indexPath = IndexPath(item: 0, section: index)
            let attributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath)
            layoutAttribute.append(attributes!)
        }
        
        for attribute in layoutAttribute {
            if let elementKind = attribute.representedElementKind {
                if elementKind == UICollectionElementKindSectionHeader {
                    let section = attribute.indexPath.section
                    let attributeForFirstItemInSection = layoutAttributesForItem(at: IndexPath(item: 0, section: section))
                    let attributeForFirstLastInSection = layoutAttributesForItem(at: IndexPath(item: collectionView!.numberOfItems(inSection: section) - 1, section: section))
                    var frame = attribute.frame
                    let offset = collectionView!.contentOffset.y
                    
                    let minY = (attributeForFirstItemInSection?.frame.minY)! - frame.height
                    let maxY = (attributeForFirstLastInSection?.frame.maxY)! - frame.height
                    let y = min(max(offset, minY), maxY)
                    frame.origin.y = y
                    attribute.frame = frame
                    attribute.zIndex = 99
                }
            }
        }
        
        return layoutAttribute
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
