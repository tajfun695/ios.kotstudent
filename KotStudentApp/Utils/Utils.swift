//
//  Utils.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 01.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class Utils {
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func DateParse(date: String?) -> Date? {
        if(date != nil){
            let dateFor: DateFormatter = DateFormatter()
            dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            return dateFor.date(from: date!)
        }
        return nil
    }
}
