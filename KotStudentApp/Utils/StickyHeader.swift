//
//  StickyHeader.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 24.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

class StickyHeaderCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var superAttributes: [UICollectionViewLayoutAttributes]? = super.layoutAttributesForElements(in: rect)
        
        if superAttributes == nil {
            // If superAttributes couldn't cast, return
            return super.layoutAttributesForElements(in: rect)
        }
        
        let contentOffset = collectionView!.contentOffset
        var missingSections = NSMutableIndexSet()
        
        for layoutAttributes in superAttributes! {
            if (layoutAttributes.representedElementCategory == .cell) {
                missingSections.add(layoutAttributes.indexPath.section)
            }
        }
        
        for layoutAttributes in superAttributes! {
            if let representedElementKind = layoutAttributes.representedElementKind {
                if representedElementKind == UICollectionElementKindSectionHeader {
                    missingSections.remove(layoutAttributes.indexPath.section)
                }
            }
        }
        
        missingSections.enumerate { idx, stop in
            let indexPath = IndexPath(item: 0, section: idx)
            if let layoutAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
                superAttributes!.append(layoutAttributes)
            }
        }
        
        for layoutAttributes in superAttributes! {
            if let representedElementKind = layoutAttributes.representedElementKind {
                if representedElementKind == UICollectionElementKindSectionHeader {
                    let section = layoutAttributes.indexPath.section
                    let numberOfItemsInSection = collectionView!.numberOfItems(inSection: section)
                    
                    let firstCellIndexPath = IndexPath(item: 0, section: section)
                    let lastCellIndexPath = IndexPath(item: max(0, (numberOfItemsInSection - 1)), section: section)
                    
                    
                    let (firstCellAttributes: _, lastCellAttributes: _) = { () -> (UICollectionViewLayoutAttributes?, UICollectionViewLayoutAttributes?) in
                        if (self.collectionView!.numberOfItems(inSection: section) > 0) {
                            return (
                                self.layoutAttributesForItem(at: firstCellIndexPath),
                                self.layoutAttributesForItem(at: lastCellIndexPath))
                        } else {
                            return (
                                self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: firstCellIndexPath),
                                self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionFooter, at: lastCellIndexPath))
                        }
                    }()
                    
                    let headerHeight = CGRectGetHeight(layoutAttributes.frame)
                    var origin = layoutAttributes.frame.origin
                    
                    origin.y = min(contentOffset.y, (CGRectGetMaxY(layoutAttributes.frame) - headerHeight))
                    // Uncomment this line for normal behaviour:
                    // origin.y = min(max(contentOffset.y, (CGRectGetMinY(firstCellAttributes.frame) - headerHeight)), (CGRectGetMaxY(lastCellAttributes.frame) - headerHeight))
                    
                    layoutAttributes.zIndex = 1024
                    layoutAttributes.frame = CGRect(origin: origin, size: layoutAttributes.frame.size)
                }
            }
        }
        return superAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
}
