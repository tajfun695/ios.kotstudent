//
//  LoginScreenView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 04.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FBSDKLoginKit

class LoginScreenView: UIViewController, FBSDKLoginButtonDelegate{
    //MARK: Variables
    let disposeBag = DisposeBag()
    var viewModel: LoginViewModel = LoginViewModel()
    
    
    //MARK: Implementations
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true;
        view.backgroundColor = Color.mainColor
        self.hideKeyboardWhenTappedAround()
        
        setupBinding()
        setupUI()
        setupConstraints()
        viewModel.LogoutFacebook()
    }
    
    fileprivate func setupBinding(){
        emailField.rx.text.orEmpty.bind(to: viewModel.email).disposed(by: disposeBag)
        passwordField.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: disposeBag)
        
        _ = viewModel.isClickable.map{ $0 }.bind(to: loginButton.rx.isEnabled)
    }
    
    fileprivate func setupUI(){
        view.addSubview(logo)
        view.addSubview(emailField)
        view.addSubview(separator)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        view.addSubview(fBloginButton)
        view.addSubview(registerLabel)
        view.addSubview(createOneLabel)
    }
    
    fileprivate func setupConstraints(){
        logo.snp.makeConstraints { (make) in
            make.width.equalTo(150)
            make.height.equalTo(150)
            make.centerX.equalTo(view.center)
            make.topMargin.equalTo(view.frame.width / 3)
        }
        
        emailField.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(24)
            make.left.equalTo(view.snp.left).offset(24)
            make.right.equalTo(view.snp.right).offset(-24)
            make.bottom.equalTo(separator.snp.top)
            make.height.equalTo(50)
        }
        
        separator.snp.makeConstraints { (make) in
            make.top.equalTo(emailField.snp.bottom)
            make.left.right.equalTo(emailField)
            make.bottom.equalTo(passwordField.snp.top)
            make.height.equalTo(1)
        }
        
        passwordField.snp.makeConstraints { (make) in
            make.top.equalTo(separator.snp.bottom)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordField.snp.bottom).offset(12)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
        }
        
        fBloginButton.snp.makeConstraints { (make) in
            make.top.equalTo(loginButton.snp.bottom).offset(12)
            make.left.right.equalTo(emailField)
            make.height.equalTo(50)
            make.centerX.equalTo(view.center)
        }
        
        registerLabel.snp.makeConstraints { (make) in
            let positionOfText = (view.frame.width - (registerLabel.intrinsicContentSize.width + createOneLabel.intrinsicContentSize.width)) / 2
            make.bottom.equalTo(view.snp.bottom).offset(-16)
            make.leftMargin.equalTo(positionOfText)
        }
        
        createOneLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.bottom).offset(-16)
            make.left.equalTo(registerLabel.snp.right).offset(4)
        }
    }
    
    @objc func handledLogin(){
        loginButton.press()
        self.viewModel.LoginViaEmail(){ result in
            switch result{
            case .success:
                self.dismiss(animated: true, completion: nil)
            case .fail(let error):
                let alert = UIAlertController(title: "Error", message: error!, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: {
                    print("\(error!)")
                })
                break
            }
        }
    }
    
    @objc func handledRegister(){
        let registerView = RegisterAccountView()
        registerView.viewModel = RegisterViewModel()
        self.present(registerView, animated: true, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil || result.isCancelled {
            return
        }
        
        viewModel.LoginViaFacebook(FBSDKAccessToken.current().tokenString){ result in
            switch result{
            case .success:
                self.dismiss(animated: true, completion: nil)
            case .fail(let error):
                let alert = UIAlertController(title: "Error", message: error!, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: {
                    print("\(error!)")
                })
                break
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    }
    
    // MARK: Components
    lazy var fBloginButton: FBSDKLoginButton = {
        let loginButton = FBSDKLoginButton()
        loginButton.delegate = self
        return loginButton
    }()
    
    var logo: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Logo"))
        view.frame = view.frame
        view.contentMode = .scaleToFill
        return view
    }()
    
    var emailField: UITextField! = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("E-mail")
        textView.text = "leonzawodowiec@gmail.com"
        textView.autocorrectionType = .no
        textView.backgroundColor = .white
        textView.keyboardType = UIKeyboardType.emailAddress
        textView.autocapitalizationType = .none
        textView.setLeftPaddingPoints(8)
        return textView
    }()
    
    var passwordField: UITextField = {
        let textView = UITextField()
        textView.placeholder = Language.getWord("Password")
        textView.text = "Qwerty123" 
        textView.backgroundColor = .white
        textView.setLeftPaddingPoints(8)
        textView.isSecureTextEntry = true;
        return textView
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Color.secondaryColor
        button.setTitle(Language.getWord("Sign In"), for: .normal)
        button.addTarget(self, action: #selector(handledLogin), for: .touchUpInside)
        return button
    }()
    
    var registerLabel: UILabel = {
        var label = UILabel()
        label.textColor = .white
        label.font = label.font.withSize(12)
        label.text = Language.getWord("Don`t have any account?")
        return label
    }()
    
    lazy var createOneLabel: UILabel = {
        var label = UILabel()
        label.textColor = Color.secondaryColor
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.attributedText = NSAttributedString(string: Language.getWord("Create One"), attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handledRegister))
        tapGestureRecognizer.numberOfTapsRequired = 1;
        label.addGestureRecognizer(tapGestureRecognizer)
        label.isUserInteractionEnabled = true;
        return label
    }()
    
    var separator: UIView = {
        var view = UIView()
        view.backgroundColor = .gray
        return view;
    }()
    
}
