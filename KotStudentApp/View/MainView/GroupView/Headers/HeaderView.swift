//
//  CollectionViewSectionHeader.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 25.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class HeaderView: UICollectionReusableView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    var delegate: GroupView?
    var threads: [Thread] = []
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        if let flow =  collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            flow.scrollDirection = .horizontal
            flow.sectionHeadersPinToVisibleBounds = true
        }
        collectionView.register(ThreadCellView.self, forCellWithReuseIdentifier: "cellId")
        collectionView.frame = self.bounds
        self.addSubview(collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! ThreadCellView
        cell.title.text = threads[indexPath.item].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return threads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 3, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.changeThread(thredID: threads[indexPath.item].id!)
    }
}
