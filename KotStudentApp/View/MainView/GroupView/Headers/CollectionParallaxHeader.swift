//
//  CollectionParallaxHeader.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 25.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class CollectionParallaxHeader: GSKStretchyHeaderView {
    
    private var coverView: UIImageView?
    private var photoView: UIImageView?
    private var groupName: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.lightGray
        self.clipsToBounds = true
        
        
        let bounds = CGRect(x:0, y:0, width: frame.maxX, height: frame.maxY)
        coverView = UIImageView(frame: bounds)
        coverView?.contentMode = UIViewContentMode.scaleAspectFill
        coverView?.image = UIImage(named: "groupDefaultCover")
        self.addSubview(coverView!)
        
        coverView?.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.width.equalTo(100)
            make.top.bottom.left.right.equalToSuperview()
        }
        
        photoView = UIImageView(frame: bounds)
        photoView?.contentMode = UIViewContentMode.scaleAspectFill
        photoView?.image = UIImage(named: "groupDefaultImage")
        coverView?.addSubview(photoView!)
        
        photoView?.snp.makeConstraints { (make) in
            make.width.height.equalTo(84)
            make.center.equalToSuperview()
        }
        
        groupName = UILabel(frame: bounds)
        groupName?.textColor = .white
        groupName?.text = "Grupa dobrego uczenia się!"
        groupName?.font = UIFont.boldSystemFont(ofSize: 24.0)
        coverView?.addSubview(groupName!)
        
        groupName?.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(photoView!.snp.bottom).offset(8)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.coverView?.frame = self.bounds
    }
    
    public func setupGroupInfo(group: Group){
        groupName?.text = group.name
    }
    
}
