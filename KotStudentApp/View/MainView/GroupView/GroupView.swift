//
//  GroupView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 23.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GSKStretchyHeaderView

enum GroupViewIds: String {
    case HeaderId = "HeaderID"
    case CellId = "CellID"
    case EmptyId = "EmptyID"
}

class GroupView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //Private variable
    private var viewWasLoaded: Bool = false
    private var downloadingPostIsExecuting: Bool = false
    private var isLoading = false
    private let disposeBag = DisposeBag()
    
    //variables
    var currentThread: Int = 0
    var groupID: Int?
    var viewModel: GroupViewModel?
    var stretchyHeader: CollectionParallaxHeader?

    //arrays
    var threads: [Thread] = []
    var posts : [Post] = []
    
    var layout: UICollectionViewLayout? {
        return collectionView?.collectionViewLayout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.backgroundColor = Color.background
        self.collectionView?.alwaysBounceVertical = true
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        if(!viewWasLoaded){
            viewWasLoaded = true
            loadLayoyt()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(posts.count != 0){
            return posts.count
        }
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(posts.count != 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GroupViewIds.CellId.rawValue, for: indexPath) as! PostViewCell
            cell.backgroundColor = .white
            cell.data = posts[indexPath.item]
            cell.thread = currentThread
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GroupViewIds.EmptyId.rawValue, for: indexPath) as! EmptyThredCell
        return cell
        
    }
    
    func loadLayoyt() {
        // --Mark-- // HEADER
        let headerSize = CGSize(width: self.collectionView!.frame.size.width, height: 240) // 200 will be the default height
        self.stretchyHeader = CollectionParallaxHeader(frame: CGRect(x: 0, y: 0, width: headerSize.width, height: headerSize.height))
        self.stretchyHeader?.setupGroupInfo(group: viewModel!.group.value)
        self.collectionView?.addSubview(self.stretchyHeader!)
        
        // --Mark-- // Setup Cell
        self.collectionView?.register(PostViewCell.self, forCellWithReuseIdentifier: GroupViewIds.CellId.rawValue)
        self.collectionView?.register(EmptyThredCell.self, forCellWithReuseIdentifier: GroupViewIds.EmptyId.rawValue)
        self.collectionView?.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: GroupViewIds.HeaderId.rawValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if posts.count != 0 {
            if let statusText = posts[indexPath.item].content {
                
                let rect = NSString(string: statusText).boundingRect(with: CGSize(width: view.frame.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
                
                if(rect.height <= 70){
                    return CGSize(width: view.frame.width, height: (rect.height) + 72)
                } else {
                    return CGSize(width: view.frame.width, height: 156)
                }
            }
        }
        return CGSize(width: view.frame.width, height: 80)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: GroupViewIds.HeaderId.rawValue, for: indexPath) as! HeaderView
        view.threads = viewModel!.threads.value
        view.delegate = self
        return view
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //getting the current cell from the index path
        guard collectionView.cellForItem(at: indexPath) is PostViewCell else { return }
        
        let currentCell = collectionView.cellForItem(at: indexPath) as! PostViewCell
        let postLayout = UICollectionViewFlowLayout()
        let postView: PostView = PostView(collectionViewLayout: postLayout)
        postView.post = currentCell.data
        
        if(!downloadingPostIsExecuting){
            downloadingPostIsExecuting = true
            postView.fetchUI(threadId: currentCell.thread!, postId: currentCell.data!.id!) { (result) in
                switch result{
                case .success:
                    let button = UIBarButtonItem()
                    button.tintColor = .black
                    self.navigationItem.backBarButtonItem = button
                    self.navigationController?.pushViewController(postView, animated: true)
                    self.downloadingPostIsExecuting = false
                case .fail(let Error):
                    print(Error!)
                    self.downloadingPostIsExecuting = false
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == posts.count - 1 ) {
            guard !self.isLoading else { return }
            self.isLoading = true
            loadModePosts(lastPost: posts.last!.id!, completion: {result in
                switch result{
                case .success:
                    self.isLoading = false
                case .fail(_):
                    print("Coś nie pykło 😒")
                }
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 30)
    }
    
    @objc func addTapped() {
        let inputView = InputPostView()
        inputView.threads = viewModel!.threads.value
        inputView.delegate = self
        self.present(inputView, animated: true, completion: nil)
        print("test")
    }
    
    func changeThread(thredID: Int) {
        self.viewModel!.posts.value = []
        viewModel?.GetPosts(threadId: thredID, completion: { (result) in
            switch result{
            case .success:
                self.currentThread = thredID
                self.posts = self.viewModel!.posts.value
                self.collectionView?.reloadData()
            case .fail(let error):
                print(error!)
            }
        })
    }
    
    func loadModePosts(lastPost: Int, completion: @escaping (Result) -> ()){
        viewModel?.GetMorePosts(threadId: currentThread, lastPost: lastPost, completion: { (result) in
            switch result {
            case .success:
                self.posts = self.viewModel!.posts.value
                self.collectionView?.reloadData()
                completion(.success)
            case .fail(let error):
                print(error!)
            }
        })
    }
    
     func fetchUI(groupId:Int, completion: @escaping (Result) -> ()){
        viewModel = GroupViewModel()
        viewModel?.GetComplesGroup(groupId: groupId) { (result) in
            switch result{
            case .success:
                self.currentThread = self.viewModel!.threads.value[0].id!
                self.viewModel?.GetPosts(threadId: self.viewModel!.threads.value[0].id!, completion: { (result) in
                    switch result{
                    case .success: self.posts = self.viewModel!.posts.value; completion(.success)
                    case .fail(let Error): print(Error!)
                    }
                })
            case .fail(let Error):
                print(Error!)
                completion(.fail("Nie paca!"))
            }
        }
    }
}
