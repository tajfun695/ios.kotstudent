//
//  CollectionViewCell.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 25.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class PostViewCell: UICollectionViewCell{
    
    var thread: Int?
    var data : Post? {
        didSet {
            self.nameUserLabel?.text = (data!.creator?.userFirstName!)! + " " + (data!.creator?.userLastName)!
            self.textLabel?.text = data!.content
            self.commentCount?.text = "Komentarzy: " + String(describing: data!.commentsCount!)
        }
    }
    
    private var textLabel: UILabel?
    private var nameUserLabel: UILabel?
    private var commentCount: UILabel?
    private var editButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blue
        
        self.nameUserLabel = UILabel()
        self.addSubview(nameUserLabel!)

        
        self.textLabel = UILabel()
        //self.textLabel?.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.textLabel?.font = UIFont.systemFont(ofSize: 14)
        self.textLabel?.numberOfLines = 5
        self.textLabel?.lineBreakMode = .byWordWrapping
        self.textLabel?.lineBreakMode = .byTruncatingTail
        self.addSubview(textLabel!)
        
        self.commentCount = UILabel()
        self.addSubview(commentCount!)
        
        self.editButton = UIButton()
        self.editButton?.setImage(UIImage(named: "moreIcon"), for: .normal)
        self.addSubview(editButton!)
        
        self.nameUserLabel?.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.left.right.equalToSuperview().offset(12)
        }
        
        self.textLabel?.snp.makeConstraints{ (make) in
            make.top.equalTo(nameUserLabel!.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-12)
        }
        
        self.commentCount?.snp.makeConstraints{ (make) in
            make.right.bottom.equalToSuperview().offset(-4)
        }
        
        self.editButton?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(12)
            make.right.equalToSuperview().offset(-8)
            make.height.equalTo(28)
            make.width.equalTo(36)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
