//
//  ThreadCellView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 16.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class ThreadCellView: BaseCell {
    
    var data: Thread? {
        didSet{
            title.text = data?.title
        }
    }
    
    override var isSelected: Bool {
        didSet {
            title.textColor = isSelected ? UIColor.white : UIColor.black
        }
    }
    
    override func setupViews(){
        self.backgroundColor = Color.secondaryColor
        addSubview(title)
        title.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    var title: UILabel = {
        let text: UILabel = UILabel()
        text.font = UIFont.systemFont(ofSize: 14.0);
        text.textColor = .black
        text.lineBreakMode = .byWordWrapping
        text.numberOfLines = 0
        return text
    }()
}
