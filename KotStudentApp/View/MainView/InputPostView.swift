//
//  InputPostView.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 15.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import SwiftyJSON

class InputPostView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private var contentTextBox: UITextView?
    private var acceptButton: UIButton?
    private var cancelButton: UIButton?
    private var threadPicker: UIPickerView?
    
    var selectedThreadID: Int?
    var threads: [Thread] = []
    var delegate: GroupView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = Color.background
        selectedThreadID = threads[0].id!
        self.hideKeyboardWhenTappedAround()
        setupLayout()
        
    }
    
    func setupLayout() {
        contentTextBox = UITextView()
        contentTextBox?.font = UIFont.systemFont(ofSize: 14)
        contentTextBox?.backgroundColor = .white
        
        threadPicker = UIPickerView()
        threadPicker?.dataSource = self;
        threadPicker?.delegate = self;
        threadPicker?.backgroundColor = .white
        
        acceptButton = UIButton()
        acceptButton?.setTitle("Dodaj", for: .normal)
        acceptButton?.backgroundColor = Color.secondaryColor
        acceptButton?.addTarget(self, action: #selector(addPost), for: .touchUpInside)
        
        cancelButton = UIButton()
        cancelButton?.setTitle("Cancel", for: .normal)
        cancelButton?.addTarget(self, action: #selector(dismissWindow), for: .touchUpInside)
        cancelButton?.backgroundColor = UIColor.lightGray
        
        
        
        self.view.addSubview(contentTextBox!)
        self.view.addSubview(acceptButton!)
        self.view.addSubview(cancelButton!)
        self.view.addSubview(threadPicker!)
        
        contentTextBox?.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(24)
            make.left.right.equalToSuperview()
            make.height.equalTo(view.frame.height / 2)
        }
        
        threadPicker?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(cancelButton!.snp.top).offset(-4)
            make.left.right.equalToSuperview()
            make.height.equalTo(100)
        })
        
        acceptButton?.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview().offset(-8)
            make.right.equalToSuperview().offset(-8)
            make.left.equalTo(cancelButton!.snp.right).offset(-4)
            make.height.equalTo(50)
            make.width.equalTo(view.frame.width / 2)
        })
        
        cancelButton?.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview().offset(-8)
            make.left.equalToSuperview().offset(8)
            make.right.equalTo(acceptButton!.snp.left).offset(-4)
            make.height.equalTo(50)
            make.width.equalTo(view.frame.width / 2)
        })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return threads.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return threads[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedThreadID = threads[row].id!
    }
    
    @objc func addPost(){
        let json = JSON(["postContent": contentTextBox!.text!, "threadId": selectedThreadID!])
        GroupViewModel.putPost(threadId: selectedThreadID!, parameters: json) { (result) in
            switch result {
            case .success:
                self.delegate?.changeThread(thredID: self.selectedThreadID!)
                self.dismiss(animated: true, completion: nil)
            case .fail(let error):
                print(error!)
            }
        }
    }
    
    @objc func dismissWindow(){
        self.dismiss(animated: true, completion: nil)
    }
}
