//
//  PostView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 22.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Cells: String {
    case Header = "Header"
    case Cell = "Cell"
}

class PostView: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    var threadId: Int?
    var post: Post?
    var comments: [Comment] = []
    var viewModel: PostViewModel?
    
    
    private var naviTitleView: UIView?
    private var naviUserName: UILabel?
    private var naviDate: UILabel?
    private var inputContenerView: UIView?
    private var inputTextBox: UITextView?
    private var sendButton: UIButton?
    private var nameUserLabel: UILabel?
    private var separator: UIView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.backgroundColor = .white
        self.collectionView?.register(PostCellView.self, forCellWithReuseIdentifier: Cells.Cell.rawValue)
        self.collectionView?.register(PostHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: Cells.Header.rawValue)
        self.hideKeyboardWhenTappedAround()
        self.setupTitleView()
        self.setupNavigationController()
        self.setupInputContainer()
            
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = .white
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func setupTitleView(){
        naviTitleView = UIView()
        
        naviUserName = UILabel()
        naviUserName?.text = post!.creator!.userFirstName! + " " + post!.creator!.userLastName!
        naviUserName?.textColor = .black
        naviUserName?.font = UIFont.systemFont(ofSize: 16)
        
        naviDate = UILabel()
        naviDate?.text = post!.createDate!.description
        naviDate?.textColor = .black
        naviDate?.font = UIFont.systemFont(ofSize: 12)
        
        naviTitleView?.addSubview(naviUserName!)
        naviTitleView?.addSubview(naviDate!)
        
        naviUserName?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(-150)
            make.right.equalToSuperview()
            make.bottom.equalTo(naviDate!.snp.top)
        })
        
        naviDate?.snp.makeConstraints({ (make) in
            make.top.equalTo(naviUserName!.snp.bottom).offset(4)
            make.left.equalTo(naviUserName!.snp.left)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        })
    }
    
    private func setupNavigationController() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.titleView = naviTitleView
        self.navigationItem.backBarButtonItem = UIBarButtonItem()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func setupInputContainer() {
        self.collectionView?.backgroundColor = Color.background
        inputContenerView = UIView()
        inputContenerView?.backgroundColor = .white
        self.view.addSubview(inputContenerView!)
        
        inputTextBox = UITextView()
        inputTextBox?.backgroundColor = .white
        inputTextBox?.font = UIFont.systemFont(ofSize: 16)
        inputTextBox?.delegate = self
        self.inputContenerView?.addSubview(inputTextBox!)
        
        sendButton = UIButton()
        sendButton?.backgroundColor = Color.secondaryColor
        sendButton?.setTitle("Send", for: .normal)
        sendButton?.addTarget(self, action: #selector(addComment), for: .touchUpInside)
        self.inputContenerView?.addSubview(sendButton!)
        
        separator = UIView()
        separator?.backgroundColor = .black
        self.inputContenerView?.addSubview(separator!)
        
        self.collectionView?.snp.makeConstraints({ (make) in
            make.top.right.left.equalToSuperview()
            make.bottom.equalTo(inputContenerView!.snp.top)
        })
        
        inputContenerView?.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(42)
        })
        
        inputTextBox?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.left.equalToSuperview().offset(8)
            make.right.equalTo(sendButton!.snp.left).offset(-4)
        })
        
        sendButton?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.left.equalTo(inputTextBox!.snp.right).offset(4)
            make.right.equalToSuperview().offset(-5)
            make.width.equalTo(self.view.frame.width / 5)
        })
        
        separator?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        })
    }
    
    @objc private func handleKeyboardNotification(notification: Notification) {
        if notification.userInfo != nil {
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                
                let isKeyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
                
                if(isKeyboardShowing == true) {
                    self.inputContenerView?.snp.updateConstraints({ (make) in
                        make.bottom.equalTo(-keyboardHeight)
                        make.left.right.equalToSuperview()
                        
                    })
                } else {
                    self.inputContenerView?.snp.updateConstraints({ (make) in
                        make.bottom.equalTo(0)
                        make.left.right.equalToSuperview()
                    })
                }
                
                UIView.animate(withDuration: 0, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (result) in
                    self.moveToLast()
                })
            }
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Liczba komentarzy: " + String(comments.count))
        return comments.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cells.Cell.rawValue, for: indexPath) as! PostCellView
        if(comments.count != 0) {
            cell.data = comments[indexPath.item]
            return cell
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Cells.Header.rawValue, for: indexPath) as! PostHeaderView
        view.data = post
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let statusText = post?.content{
            
            let rect = NSString(string: statusText).boundingRect(with: CGSize(width: view.frame.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)], context: nil)
            
            return CGSize(width: view.frame.width, height: rect.height + 56)
        }
        
        return CGSize(width: view.frame.width, height: 500)
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if(parent == nil) {
            self.navigationController?.navigationBar.barTintColor = Color.mainColor
            guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
                return
            }
            statusBar.backgroundColor = Color.mainColor
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            self.navigationController?.hidesBarsOnSwipe = false
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    @objc func addComment(){
        let json = JSON(["content": inputTextBox?.text!])
        PostViewModel.putComment(threadID: self.threadId!, postID: self.post!.id! , parameters: json) { (result) in
            switch result {
            case .success:
                self.reloadComment()
                self.inputTextBox?.text = ""
            case .fail(let error):
                print(error!)
            }
        }
    }
    
    func moveToLast(){
        if(self.comments.count > 2) {
            let indexPath: IndexPath = IndexPath(item: self.comments.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexPath , at: .bottom, animated: true)
        }
    }
    
    func reloadComment(){
        self.viewModel!.comments.value = []
        self.viewModel!.GetComment(threadId: self.threadId!, postId: self.post!.id!, completion: { (result) in
            switch result {
            case .success:
                self.comments = self.viewModel!.comments.value
                self.collectionView?.reloadData()
                self.moveToLast()
            case .fail(let error):
                print(error!)
            }
        })
    }
    
    func fetchUI(threadId: Int, postId:Int, completion: @escaping (Result) -> ()){
        viewModel = PostViewModel()
        viewModel?.GetComment(threadId: threadId, postId: postId){ (result) in
            switch result{
            case .success:
                self.threadId = threadId
                self.comments = self.viewModel!.comments.value
                self.collectionView?.reloadData()
                completion(.success)
            case .fail(let Error):
                print(Error!)
                completion(.fail("Nie paca!"))
            }
        }
    }
}
