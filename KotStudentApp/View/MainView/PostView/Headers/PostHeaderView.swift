//
//  PostHeaderView.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 13.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class PostHeaderView: UICollectionReusableView {
    
    private var textLabel: UILabel?
    private var separator: UIView?
    
    var data: Post? {
        didSet{
            textLabel?.text = data!.content
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        textLabel = UILabel()
        textLabel?.numberOfLines = 0
        textLabel?.lineBreakMode = .byWordWrapping
        textLabel?.font = UIFont.systemFont(ofSize: 16)
        
        separator = UIView()
        separator?.backgroundColor = .black
        self.addSubview(textLabel!)
        self.addSubview(separator!)
        
        textLabel?.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(18)
            make.right.equalToSuperview().offset(-18)
        }
        
        separator?.snp.makeConstraints({ (make) in
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
            make.height.equalTo(1)
        })
    }
}
