//
//  PostCellView.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 13.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class PostCellView: UICollectionViewCell{
    
    private var textLabel: UILabel?
    private var nameUserLabel: UILabel?
    private var dateLabel: UILabel?
    
    var data: Comment? {
        didSet{
            self.nameUserLabel?.text = data!.creator!.userFirstName! + " " + data!.creator!.userLastName!
            self.textLabel?.text = data?.content
            self.dateLabel?.text = data!.createDate!.description
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        textLabel = UILabel()
        textLabel?.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(textLabel!)
        
        nameUserLabel = UILabel()
        nameUserLabel?.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(nameUserLabel!)
        
        dateLabel = UILabel()
        dateLabel?.font = UIFont.systemFont(ofSize: 12)
        self.addSubview(dateLabel!)
        
        nameUserLabel?.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(12)
        }
        
        textLabel?.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel!.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(12)
        }
        
        dateLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(nameUserLabel!.snp.bottom).offset(4)
            make.left.equalTo(nameUserLabel!.snp.left)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
