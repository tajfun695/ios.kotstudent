//
//  DashboardView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 24.02.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class DashboardView : UICollectionViewController, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.perform(#selector(loginToService))
        navigationItem.title = "Dashboard"
        collectionView?.backgroundColor = Color.background
        collectionView?.register(DashboardCellView.self, forCellWithReuseIdentifier: "DashboardViewCellID")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardViewCellID", for: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
    @objc fileprivate func loginToService(){
        let loginView = LoginScreenView()
        self.present(loginView, animated: true)
    }
}
