//
//  DashboardCellView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 23.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class DashboardCellView: BaseCell {
    override func setupViews(){
        self.backgroundColor = Color.cellColor
        addSubview(photo)
        addSubview(userName)
        addSubview(date)
        addSubview(planeText)
        
        photo.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(8)
            make.height.width.equalTo(50)
        }
        
        userName.snp.makeConstraints { (make) in
            make.left.equalTo(photo.snp.right).offset(8)
            make.top.equalTo(photo)
        }
        
        date.snp.makeConstraints { (make) in
            make.left.equalTo(userName).offset(4)
            make.top.equalTo(userName.snp.bottom).offset(4)
        }
        
        planeText.snp.makeConstraints { (make) in
            make.top.equalTo(photo.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
            make.bottom.equalToSuperview().offset(-4)
        }
    }
    
    
    let photo: UIImageView = {
        let image: UIImageView = UIImageView()
        image.backgroundColor = .blue
        return image
    }()
    
    let userName: UILabel = {
        let text: UILabel = UILabel()
        text.text = "Bartłomiej Łaski"
        text.font = UIFont.systemFont(ofSize: 14.0);
        text.backgroundColor = .yellow
        return text
    }()
    
    let date: UILabel = {
        let text: UILabel = UILabel()
        text.text = "23.12.2018"
        text.font = UIFont.systemFont(ofSize: 11.0);
        text.backgroundColor = .green
        return text
    }()
    
    let planeText: UILabel = {
        let text: UILabel = UILabel()
        text.text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        text.font = UIFont.systemFont(ofSize: 16.0);
        text.lineBreakMode = .byWordWrapping
        text.numberOfLines = 0
        text.backgroundColor = .red
        return text
    }()
    
}
