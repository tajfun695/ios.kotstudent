//
//  GroupsCellView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 15.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import SnapKit

class GroupsCellView: BaseCell {
    
    override var isHighlighted: Bool {
        didSet {
            self.backgroundColor = isHighlighted ? Color.lightGrey : Color.cellColor
        }
    }
    
    var data: Group? {
        didSet{
            groupName.text = data?.name
        }
    }
    
    override func setupViews() {
        self.backgroundColor = Color.cellColor
        self.addSubview(groupName)
        self.addSubview(arrow)
        
        groupName.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(12)
            make.right.equalTo(arrow.snp.left)
        }
        arrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-12)
            make.height.width.equalTo(24)
        }
    }
    
    let groupName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18.0);
        return label
    }()
    
    let arrow: UIImageView = {
        let image = UIImageView(image: UIImage(named: "arrow-point-to-right"))
        return image
    }()
}
