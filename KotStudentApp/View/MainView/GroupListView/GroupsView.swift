
//
//  GroupView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 14.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GroupsView: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var groups: [Group] = []
    var viewModel: GroupsViewModel?
    var searchBar: UISearchBar?
    var downloadingPostIsExecuting: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Groups"
        self.collectionView?.backgroundColor = Color.background
        self.collectionView?.register(GroupsCellView.self, forCellWithReuseIdentifier: "GroupsViewCellID")
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            self.fillUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.perform(#selector(loginToService))
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupsViewCellID", for: indexPath) as! GroupsCellView
//        cell.groupName.text = viewModel.groups.value.GroupsItems?[indexPath.item].groupName
        cell.alpha = 0.0
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1.0
        }
        cell.data = groups[indexPath.item]
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groups.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //getting the current cell from the index path
        let currentCell = collectionView.cellForItem(at: indexPath) as! GroupsCellView
        
        let layout = StickyHeadersLayout()
        let groupView: GroupView = GroupView(collectionViewLayout: layout)
        groupView.groupID = currentCell.data?.id!
        
        if(!downloadingPostIsExecuting){
            downloadingPostIsExecuting = true
            groupView.fetchUI(groupId: currentCell.data!.id!) { (result) in
                switch result{
                case .success:
                    self.navigationController?.pushViewController(groupView, animated: true)
                    self.downloadingPostIsExecuting = false
                case .fail(let Error):
                    print(Error!)
                    self.downloadingPostIsExecuting = false
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func fillUI(){
        viewModel = GroupsViewModel()
        viewModel?.ReloadGroups(completion: { (result) in
            switch result{
            case .success:
                self.groups = self.viewModel!.groups.value
                self.collectionView?.reloadData()
            case .fail(let Error):
                print(Error!)
            }
        })
    }
    
    @objc fileprivate func loginToService(){
        let loginView = LoginScreenView()
        self.present(loginView, animated: true)
    }
}
