//
//  BaseCell.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 11.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
