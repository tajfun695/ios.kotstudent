//
//  ScreenLoader.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 06.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

enum ViewType {
    case LoadingScreen
    case LoginScreen
}
