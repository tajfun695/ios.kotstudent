//
//  EnglishLanguage.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 03.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

class EnglishLanguage {
    func getLanguage() -> [String: String]{
        var dictionary: [String: String] = [:]
        
        dictionary["Sign In"] = "Sign In"
        dictionary["Sign Up"] = "Sign Up"
        dictionary["Don`t have any account?"] = "Don`t have any account?"
        dictionary["Create One"] = "Create One"
        dictionary["E-mail"] = "E-mail"
        dictionary["Password"] = "Password"
        dictionary["UserName"] = "Username"
        dictionary["HaveAccount"] = "I already have an account."
        
        return dictionary
    }
}
