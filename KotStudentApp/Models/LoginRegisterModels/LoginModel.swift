//
//  LoginModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 07.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

public struct LoginModel: Codable {
    public var email: String
    public var password: String
    
    init(email:String, password:String){
        self.email = email
        self.password = password
    }
}
