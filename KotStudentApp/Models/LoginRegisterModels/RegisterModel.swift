//
//  RegisterModel.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 07.03.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

public struct RegisterModel: Codable {
    public var email: String
    public var password: String
    public var username: String
    
    init(email: String, username: String, password: String) {
        self.email = email
        self.username = username
        self.password = password
    }
}
