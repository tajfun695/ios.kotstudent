//
//  AppView.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 15.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

public struct GroupInfo {
    var id: Int?
    var name: String?
    var nameAliases: [String]?
    var description: String?
    var photoUrl: String?
    var coverUrl: String?
    var createDate: Date?
    var endDate: Date?
    var opened: Bool?
    var memberCounter: Int?
    var type: String?
    var creator: User?
    var group_path: String?
    var isObservates: Bool?
    var notificationsSettings: NotificationsSettings?
    var parent: Parent?
    
    init(json: JSON){
        id = json["id"].intValue
        name = json["name"].stringValue
        nameAliases = json["nameAliases"].arrayValue as? [String?]
        description = json["description"].stringValue
        photoUrl = json["photoUrl"].stringValue
        coverUrl = json["coverUrl"].stringValue
        createDate = DateParse(json["createDate"].stringValue)
        endDate = DateParse(json["endDate"].stringValue)
        opened = json["opened"].boolValue
        memberCounter = json["memberCounter"].intValue
        type = json["type"].stringValue
        creator = User(json["creator"])
        group_path = json["group_path"].stringValue
        isObservates = json["isObservates"].boolValue
        notificationsSettings = NotificationsSettings(json["notificationsSettings"])
        parent = Parent(json["Parent"])
    }
}


