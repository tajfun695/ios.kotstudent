//
//  Parent.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 21.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

public struct Parent {
    var id: Int?
    var name: String?
    var type: String?
    var group_path: String?
    
    init(json:JSON){
        id = json["id"].intValue
        name = json["name"].stringValue
        type = json["type"].stringValue
        group_path = json["group_path"].stringValue
    }
}
