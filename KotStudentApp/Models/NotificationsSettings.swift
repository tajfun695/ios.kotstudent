//
//  NotificationSettings.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 16.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct NotificationsSettings {
    var new_post: Bool?
    var post_edited: Bool?
    var post_commented: Bool?
    var own_post_commented: Bool?
    var file_uploaded: Bool?
    var file_commented: Bool?
    var own_file_commented: Bool?
    
    init(){
        
    }
    
    init(json: JSON){
        self.new_post = json["new_post"].boolValue
        self.post_edited = json["post_edited"].boolValue
        self.post_commented = json["post_commented"].boolValue
        self.own_post_commented = json["own_post_commented"].boolValue
        self.file_uploaded = json["file_uploaded"].boolValue
        self.file_commented = json["file_commented"].boolValue
        self.own_file_commented = json["own_file_commented"].boolValue
    }
}


