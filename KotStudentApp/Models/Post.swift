//
//  Post.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 01.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct Post {
    var id: Int?
    var content: String?
    var createDate: Date?
    var modifyDate: Date?
    var commentsCount: Int?
    var creator: User?
    
    init(){
        
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        content = json["content"].stringValue
        createDate = Utils.DateParse(date: json["createDate"].stringValue)
        modifyDate = Utils.DateParse(date: json["modifyDate"].stringValue)
        commentsCount = json["commentsCount"].intValue
        creator = User(json: json["creator"])
    }
    
}

