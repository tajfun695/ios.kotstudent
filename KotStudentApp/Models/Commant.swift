//
//  Commant.swift
//  KotStudentApp
//
//  Created by Bartomiej Łaski on 13.05.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

class Comment {
    var id: Int?
    var content: String?
    var createDate: Date?
    var modifyDate: Date?
    var creator: User?
    
    init(json: JSON){
        id = json["id"].intValue
        content = json["content"].stringValue
        createDate = Utils.DateParse(date: json["createDate"].stringValue)
        modifyDate = Utils.DateParse(date: json["modifyDate"].stringValue)
        creator = User(json: json["creator"])
    }
}
