//
//  Group.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 16.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct Group {
    var id: Int?
    var name: String?
    var nameAliases: [String]?
    var description: String?
    var photoUrl: String?
    var coverUrl: String?
    var createDate: Date?
    var endDate: Date?
    var opened: Bool?
    var memberCounter: Int?
    var creator: User?
    var group_path: String?
    var isObservates: Bool?
    var notificationsSettings: NotificationsSettings?
    var parent: Parent?
    
    init() {
        
    }
    
    init(json: JSON){
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.nameAliases = json["nameAliases"].arrayObject as? [String]
        self.description = json["description"].stringValue
        self.photoUrl = json["photoUrl"].stringValue
        self.coverUrl = json["coverUrl"].stringValue
        self.createDate = Utils.DateParse(date: json["createDate"].stringValue)
        self.endDate = Utils.DateParse(date: json["endDate"].stringValue)
        self.opened = json["opened"].boolValue
        self.memberCounter = json["memberCounter"].intValue
        self.creator = User(json: json["creator"])
        self.group_path = json["group_path"].stringValue
        self.isObservates = json["isObservates"].boolValue
        self.notificationsSettings = NotificationsSettings(json: json["notificationsSettings"])
        self.parent = Parent(json: json["parent"])
    }
}

//"group": {
//    "id": 5,
//    "name": "Informatyka III rok stopień I",
//    "nameAliases": [],
//    "description": "Informatyka III rok stopień 1",
//    "photoUrl": null,
//    "coverUrl": null,
//    "createDate": "2017-03-21T19:00:24.000Z",
//    "endDate": null,
//    "opened": true,
//    "memberCounter": 12,
//    "creator": null,
//    "group_path": "/uniwersytet-lodzki/wydzial-matematyki-i-informatyki/informatyka-iii-rok-stopien-i",
//    "isObservates": true,
//    "notificationsSettings": {
//        "new_post": true,
//        "post_edited": true,
//        "post_commented": true,
//        "own_post_commented": true,
//        "file_uploaded": true,
//        "file_commented": true,
//        "own_file_commented": true
//    }
//},



