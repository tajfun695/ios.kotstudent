//
//  Thread.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 16.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct Thread {
    var id: Int?
    var title: String?
    var createDate: Date?
    var modifyDate: Date?
    var commentsCount: Int?
    var creator: User?
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.title = json["title"].stringValue
        self.createDate = Utils.DateParse(date: json["createDate"].stringValue)
        self.modifyDate = Utils.DateParse(date: json["modifyDate"].stringValue)
        self.commentsCount = json["commentsCount"].intValue
        self.creator = User(json: json["creator"])
    }
}
