//
//  User.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 11.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct User {
    var userId: Int?
    var userName: String?
    var userEmailAddress: String?
    var userRegisterDate: Date?
    var userFirstName: String?
    var userMiddleName: String?
    var userLastName: String?
    var userBirthdayDate: Date?
    var userPhotoUrl: String?
    var userCoverUrl: String?
    var userGender: String?
    var userOptions: UserOptions?
    var memberships: [Group]?
    
    init(){
        
    }
    
    init(json: JSON) {
        self.userId = json["userId"].intValue
        self.userName = json["userName"].stringValue
        self.userEmailAddress = json["userEmailAddress"].stringValue
        self.userRegisterDate = Utils.DateParse(date: json["userRegisterDate"].stringValue)
        self.userFirstName = json["userFirstName"].stringValue
        self.userMiddleName = json["userMiddleName"].stringValue
        self.userLastName = json["userLastName"].stringValue
        self.userBirthdayDate = Utils.DateParse(date: json["userBirthdayDate"].stringValue)
        self.userPhotoUrl = json["userPhotoUrl"].stringValue
        self.userCoverUrl = json["userCoverUrl"].stringValue
        self.userGender = json["userGender"].stringValue
        self.userOptions = UserOptions(json: json["userOptions"])
    }
    

}


//    "creator": {
//    "userId": 1,
//    "userName": "Bartłomiej Łaski",
//    "userEmailAddress": null,
//    "userRegisterDate": null,
//    "userFirstName": "Bartłomiej",
//    "userMiddleName": "null",
//    "userLastName": "Łaski",
//    "userBirthdayDate": null,
//    "userPhotoUrl": null,
//    "userCoverUrl": null,
//    "userGender": "male"
//    },
