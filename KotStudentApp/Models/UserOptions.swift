//
//  UserOptions.swift
//  KotStudentApp
//
//  Created by Bartłomiej Łaski on 11.04.2018.
//  Copyright © 2018 Bartłomiej Łaski. All rights reserved.
//

import SwiftyJSON

struct UserOptions {
    var showWelcomeScreen: Bool?
    var showEmailAddress: Bool?
    
    init(json: JSON) {
        self.showWelcomeScreen = json["showWelcomeScreen"].boolValue
        self.showEmailAddress = json["showEmailAddress"].boolValue
    }
}
